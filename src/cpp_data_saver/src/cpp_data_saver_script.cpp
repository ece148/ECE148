#include <iostream>
#include <chrono>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/image.hpp"
#include "opencv2/core/mat.hpp"
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <cv_bridge/cv_bridge.h>
using std::placeholders::_1;

class DataSaver : public rclcpp::Node{
public:
	DataSaver() : Node("cpp_data_saver"){
		this->img_sub_ = this->create_subscription<sensor_msgs::msg::Image>(
				"image_raw", 10, std::bind(&DataSaver::topic_callback,
					this, _1));
	}
	int i = 0;
private:
	void topic_callback(const sensor_msgs::msg::Image& msg) {
		std::cout << "received an Image" << std::endl;
		int img_height = msg.height;
		int img_width = msg.width;

		cv_bridge::CvImagePtr img_ptr = cv_bridge::toCvCopy(msg, msg.encoding);
		cv::Mat frame = img_ptr->image;
		if(i % 5 == 0){
			cv::imwrite("/home/jetson/dev_ws/recorded_data/" + std::to_string(i) + ".jpg", frame);
		}
		this->i++;
		return;
	}
	rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr img_sub_;
};

int main(int argc, char** argv){
	rclcpp::init(argc, argv);
	rclcpp::spin(std::make_shared<DataSaver>());
	rclcpp::shutdown();
	return 0;
}
