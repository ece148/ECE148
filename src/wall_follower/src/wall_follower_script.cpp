#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <math.h>
#include <utility>
#include <vector>
#include <iostream>
#include <thread>
#include <mutex>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int16.hpp"
#include "std_msgs/msg/u_int16.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/float64.hpp"

#include "tritonairacer_interfaces/msg/triton_ai_racer_control.hpp"
#include "tritonairacer_interfaces/msg/race_state.hpp"
#include "tritonairacer_interfaces/srv/e_stop.hpp"
#include "tritonairacer_interfaces/srv/change_mode.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"

#include "wall_follower/buffer_subscriber.hpp"

std::mutex mtx;

using namespace std::placeholders;
using tritonairacer_interfaces::msg::TritonAIRacerControl;
using tritonairacer_interfaces::srv::EStop;
using tritonairacer_interfaces::msg::RaceState;
using tritonairacer_interfaces::srv::ChangeMode;
using tritonairacer_interfaces::msg::VehicleMode;

using namespace std::placeholders;


class Wall_Follower : public rclcpp::Node{
private:
    // publisher
    rclcpp::Publisher<TritonAIRacerControl>::SharedPtr pub_wall_following_cmd;

    // buffer subscriber
	MsgSubscriber<sensor_msgs::msg::LaserScan>::UniquePtr lidar_sub;

    // timer
	rclcpp::TimerBase::SharedPtr step_timer_;

    // parameter
	double dt_;
	rclcpp::Time last_step_time_;
    
    // msg struct
    TritonAIRacerControl wall_following_control_cmd;
	sensor_msgs::msg::LaserScan Lidar_measurement;
public:	
	explicit Wall_Follower(const rclcpp::NodeOptions & options) : rclcpp::Node("wall_follower", options){
        std::cout << "wall_follower is constructed" << std::endl;

        declare_parameter("dt", 0.1);
		get_parameter("dt", this->dt_);


        this->pub_wall_following_cmd = this->create_publisher<TritonAIRacerControl>("autonomous_vehicle_cmd", 1);

        this->step_timer_ = rclcpp::create_timer(this, get_clock(), std::chrono::duration<float>(this->dt_), [this] {step();});
        subscribe_from(this, lidar_sub, "/scan");
	}

    // start_index and finish_index inclusive
    double avg_lidar_rays_measurement(int start_index, int finish_index){
        double sum = 0.0;
        int count = 0;
        for(int i = start_index; i < finish_index + 1; ++i){
            if(!isinf(Lidar_measurement.ranges[i])){
                sum += this->Lidar_measurement.ranges[i];
                count += 1;
            }
        }
        return (sum/count);
    }

    void step(){
		mtx.lock();
		//std::cout << "in step" << std::endl;
		if(this->lidar_sub->has_msg()){
			this->Lidar_measurement = *(this->lidar_sub->take());
			// std::cout << "angle_min: " << this->Lidar_measurement.angle_min << std::endl;
			// std::cout << "angle_max: " << this->Lidar_measurement.angle_max << std::endl;
			// std::cout << "angle_increment: " << this->Lidar_measurement.angle_increment << std::endl;
			// std::cout << "range_min: " << this->Lidar_measurement.range_min << std::endl;
			// std::cout << "range_max: " << this->Lidar_measurement.range_max << std::endl;
            //std::cout << "ranges array size: " << (sizeof(Lidar_measurement.ranges)/sizeof(Lidar_measurement.ranges[0])) << std::endl;
			//std::cout << "ranges[0]: " << this->Lidar_measurement.ranges[0] << std::endl;
			//std::cout << "ranges[449]: " << this->Lidar_measurement.ranges[449] << std::endl;
            double right_distance = avg_lidar_rays_measurement(336-5, 336+5);
            double front_distance = (avg_lidar_rays_measurement(449-4, 449) + avg_lidar_rays_measurement(0, 0+5))/2;
            //std::cout << "front distance: " << front_distance << std::endl;
            //std::cout << "right distance: " << right_distance << std::endl;
            if (front_distance < 1.0){
                this->wall_following_control_cmd.steering_openloop.steer = -0.5;
                this->wall_following_control_cmd.throttle.throttle = 15.0;
            }
            else{
                if(right_distance > 0.6){
                    this->wall_following_control_cmd.steering_openloop.steer = 0.1;
                    this->wall_following_control_cmd.throttle.throttle = 15.0;
                }
                else if(right_distance < 0.4){
                    this->wall_following_control_cmd.steering_openloop.steer = -0.1;
                    this->wall_following_control_cmd.throttle.throttle = 15.0;
                }
                else{
                    this->wall_following_control_cmd.steering_openloop.steer = 0.0;
                    this->wall_following_control_cmd.throttle.throttle = 15.0;
                }
            }
            this->pub_wall_following_cmd->publish(this->wall_following_control_cmd);
		}
        else{
            this->wall_following_control_cmd.steering_openloop.steer = 0.0; // negative is left, positive is right
            this->wall_following_control_cmd.throttle.throttle = 15.0;
            this->pub_wall_following_cmd->publish(this->wall_following_control_cmd);
        }
		mtx.unlock();
	}

};

int main(int argc, char** argv){
	rclcpp::init(argc, argv);
	rclcpp::NodeOptions options{};
	rclcpp::spin(std::make_shared<Wall_Follower>(options));
	rclcpp::shutdown();
	return 0;
}