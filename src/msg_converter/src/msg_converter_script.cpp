#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <math.h>
#include <utility>
#include <vector>
#include <iostream>
#include <thread>
#include <mutex>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int16.hpp"
#include "std_msgs/msg/u_int16.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/float64.hpp"

#include "tritonairacer_interfaces/msg/triton_ai_racer_control.hpp"
#include "tritonairacer_interfaces/msg/race_state.hpp"
#include "tritonairacer_interfaces/srv/e_stop.hpp"
#include "tritonairacer_interfaces/srv/change_mode.hpp"

std::mutex mtx;

using namespace std::placeholders;
using tritonairacer_interfaces::msg::TritonAIRacerControl;
using tritonairacer_interfaces::srv::EStop;
using tritonairacer_interfaces::msg::RaceState;
using tritonairacer_interfaces::srv::ChangeMode;
using tritonairacer_interfaces::msg::VehicleMode;

using namespace std::placeholders;

class Msg_Converter : public rclcpp::Node{
private:
	uint32_t control_publisher_freq;
	rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pub_speed_;
	rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr pub_servo_;
	rclcpp::TimerBase::SharedPtr control_timer_;
	rclcpp::Subscription<TritonAIRacerControl>::SharedPtr sub_control_;
public:	
	Msg_Converter() : rclcpp::Node("msg_converter"){
		std::cout << "msg_converter is running, it will pass pilot output cmd to vesc driver" << std::endl;
		//declare_parameter("control_publisher_freq", 20);
		//get_parameter("control_publisher_freq", control_publisher_freq);
		this->pub_speed_ = create_publisher<std_msgs::msg::Float64>("commands/motor/speed", 1);
		this->pub_servo_ = create_publisher<std_msgs::msg::Float64>("commands/servo/position", 1);
		//this->control_timer_ = this->create_wall_timer(std::chrono::milliseconds((uint32_t)(1.0 / control_publisher_freq * 1000)), std::bind(&Msg_Converter::send_control_callback, this));
		this->sub_control_ = this->create_subscription<TritonAIRacerControl>("vehicle_cmd", 1, std::bind(&Msg_Converter::receive_vehicle_cmd, this, std::placeholders::_1));
	}

	void receive_vehicle_cmd(const TritonAIRacerControl msg){
		mtx.lock();
        std_msgs::msg::Float64 servo_number;
		double steering_gain = 1;
		double steering_bias = 0.06;
        servo_number.data = 0.5 + (steering_gain * msg.steering_openloop.steer) + steering_bias; // -0.25 or 0.25
		this->pub_servo_->publish(servo_number);
		
		std_msgs::msg::Float64 speed_number;
		double throttle_gain = 1;
		double throttle_bias = 0;
		speed_number.data = 23250.0 * throttle_gain * (msg.throttle.throttle / 100.0) + throttle_bias;
		//else if(msg.brake.brake > 0){
			//speed_number.data = -23250.0 * msg.brake.brake / 100.0;
		//}
		this->pub_speed_->publish(speed_number);
		mtx.unlock();
	}

	void send_control_callback(){
		std_msgs::msg::Float64 number;
		number.data = 0.8;
		this->pub_speed_->publish(number);
	}

};

int main(int argc, char** argv){
	rclcpp::init(argc, argv);
	rclcpp::spin(std::make_shared<Msg_Converter>());
	rclcpp::shutdown();
	return 0;
}
