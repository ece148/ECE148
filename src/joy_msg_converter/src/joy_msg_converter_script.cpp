#include <memory>
#include <iostream>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include <sensor_msgs/msg/joy.hpp>

#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <math.h>
#include <utility>
#include <vector>
#include <iostream>
#include <thread>
#include <mutex>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int16.hpp"
#include "std_msgs/msg/u_int16.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/float64.hpp"

#include "tritonairacer_interfaces/msg/triton_ai_racer_control.hpp"
#include "tritonairacer_interfaces/msg/race_state.hpp"
#include "tritonairacer_interfaces/srv/e_stop.hpp"
#include "tritonairacer_interfaces/srv/change_mode.hpp"

std::mutex mtx;

using tritonairacer_interfaces::msg::TritonAIRacerControl;
using tritonairacer_interfaces::srv::EStop;
using tritonairacer_interfaces::msg::RaceState;
using tritonairacer_interfaces::srv::ChangeMode;
using tritonairacer_interfaces::msg::VehicleMode;


using std::placeholders::_1;

class Joy_Msg_Converter : public rclcpp::Node{
	public:
		Joy_Msg_Converter() : Node("Joy_Msg_Converter"){
			this->subscription_ = this->create_subscription<sensor_msgs::msg::Joy>(
					"/joy", 1, std::bind(&Joy_Msg_Converter::topic_callback, this, _1));
			this->joy_stick_pub_ = this->create_publisher<TritonAIRacerControl>("manual_vehicle_cmd", 1);
		}
	private:
		void topic_callback(const sensor_msgs::msg::Joy::SharedPtr msg) {
			//RCLCPP_INFO(this->get_logger(), "axes[0]: '%f'\n", msg->axes[0]);
			//RCLCPP_INFO(this->get_logger(), "axes[1]: '%f'\n", msg->axes[1]);
			//RCLCPP_INFO(this->get_logger(), "axes[2]: '%f'\n", msg->axes[2]);
			//RCLCPP_INFO(this->get_logger(), "axes[3]: '%f'\n", msg->axes[3]);
			this->joystick_control_cmd.steering_openloop.steer = -(msg->axes[3]) * 0.35;
			//if(msg->axes[1] >= 0.0){
			this->joystick_control_cmd.throttle.throttle = (msg->axes[1]) * 100;
			//this->joystick_control_cmd.brake.brake = 0;
			//}
			//if(msg->axes[1] < 0.0){
				//this->joystick_control_cmd.throttle.throttle = 0;
				//this->joystick_control_cmd.brake.brake = -(msg->axes[1]) * 100;
			//}
			if(msg->axes[7] <= 1.0 + 0.0001 && 1.0 - 0.0001 <= msg->axes[7]){
				this->joystick_control_cmd.throttle.throttle = 10;
			}
			this->joy_stick_pub_->publish(this->joystick_control_cmd);
		}
		rclcpp::Subscription<sensor_msgs::msg::Joy>::SharedPtr subscription_;
		TritonAIRacerControl joystick_control_cmd;
		rclcpp::Publisher<TritonAIRacerControl>::SharedPtr joy_stick_pub_;
};

int main(int argc, char * argv[]){
	rclcpp::init(argc, argv);
	rclcpp::spin(std::make_shared<Joy_Msg_Converter>());
	rclcpp::shutdown();
	return 0;
}
