from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
import os

def generate_launch_description():
    vesc_config = os.path.join(get_package_share_directory('vesc_driver'),'params','vesc_config.yaml')
    return LaunchDescription([
        Node(
            package='pilot',
            namespace='',
            executable='pilot_script',
            name='pilot'
        ),
        Node(
            package='rosboard',
            namespace='',
            executable='rosboard_node',
            name='rosboard'
        ),
        Node(
            package='msg_converter',
            namespace='',
            executable='msg_converter_script',
            name='msg_converter'
        ),
        Node(
            package='joy_msg_converter',
            namespace='',
            executable='joy_msg_converter_script',
            name='joy_msg_converter'
        ),
        #==============================vesc==========================
        Node(
            package='vesc_driver',
            executable='vesc_driver_node',
            name='vesc_driver_node',
            parameters= [vesc_config]
        ),
        #============================================================
        #==============================lidar=========================
        DeclareLaunchArgument(
            name='serial_port',
            default_value='',
            description='LD06 Serial Port'
        ),
        DeclareLaunchArgument(
            name='topic_name',
            default_value='scan',
            description='LD06 Topic Name'
        ),
        DeclareLaunchArgument(
            name='lidar_frame',
            default_value='laser',
            description='Lidar Frame ID'
        ),
        DeclareLaunchArgument(
            name='range_threshold',
            default_value='0.005',
            description='Range Threshold'
        ),
        Node(
            package='ldlidar',
            executable='ldlidar',
            name='ldlidar',
            output='screen',
            parameters=[
                {'serial_port': LaunchConfiguration("serial_port")},
                {'topic_name': LaunchConfiguration("topic_name")},
                {'lidar_frame': LaunchConfiguration("lidar_frame")},
                {'range_threshold': LaunchConfiguration("range_threshold")}
            ]
        ),
        #=============================================================
    ])
