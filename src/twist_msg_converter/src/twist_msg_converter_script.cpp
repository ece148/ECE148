#include <iostream>
#include <chrono>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <stdlib.h> 
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/image.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include "sensor_msgs/msg/imu.hpp"
#include "opencv2/core/mat.hpp"
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <cv_bridge/cv_bridge.h>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <Eigen/LU>
#include <Eigen/QR>
#include <mutex>
#include <tuple>
#include "tritonairacer_interfaces/msg/triton_ai_racer_control.hpp"
#include "tritonairacer_interfaces/msg/race_state.hpp"
#include "tritonairacer_interfaces/srv/e_stop.hpp"
#include "tritonairacer_interfaces/srv/change_mode.hpp"
#include "geometry_msgs/msg/vector3.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include <fstream>
#include <random>
#include <time.h>
#include "std_msgs/msg/bool.hpp"
using std::placeholders::_1;
std::mutex mtx;

using tritonairacer_interfaces::msg::TritonAIRacerControl;
using tritonairacer_interfaces::srv::EStop;
using tritonairacer_interfaces::msg::RaceState;
using tritonairacer_interfaces::srv::ChangeMode;
using tritonairacer_interfaces::msg::VehicleMode;

class twist_msg_converter : public rclcpp::Node{
protected:
    // subscribers
    rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr twist_sub_;
    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr pos_pixels_over_thresh_sub_;

    // publishers
    rclcpp::Publisher<TritonAIRacerControl>::SharedPtr cmd_pub_;

    // parameters
    bool pos_pixels_over_threshold;
public:
    explicit twist_msg_converter(const rclcpp::NodeOptions & options): Node("twist_msg_converter", options){
        std::cout << "in twist_msg_converter constructor" << std::endl;
        pos_pixels_over_threshold = false;
        this->twist_sub_ = this->create_subscription<geometry_msgs::msg::Twist>("/cmd_vel", 1, std::bind(&twist_msg_converter::receiveTwist, this, _1));
        this->cmd_pub_ = this->create_publisher<TritonAIRacerControl>("autonomous_vehicle_cmd", 1);
        this->pos_pixels_over_thresh_sub_ = this->create_subscription<std_msgs::msg::Bool>("/pos_pixels_over_thresh", 10, std::bind(&twist_msg_converter::receivePosPixelsTheshold, this, _1));
    }

    void receiveTwist(const geometry_msgs::msg::Twist msg) {
		mtx.lock();
        std::cout << "received twist" << std::endl;
		std::cout << msg.linear.x << std::endl;
        std::cout << msg.angular.z << std::endl;

        TritonAIRacerControl cmd;
        if(!this->pos_pixels_over_threshold){
            cmd.throttle.throttle = (msg.linear.x+0.08)*100.0;
            cmd.steering_openloop.steer = msg.angular.z*0.35;
        }
        else{
            cmd.throttle.throttle = (msg.linear.x+0.25)*100.0;
            cmd.steering_openloop.steer = msg.angular.z*0.35;
        }
        this->cmd_pub_->publish(cmd);

		mtx.unlock();
	}  

    void receivePosPixelsTheshold(std_msgs::msg::Bool msg){
        mtx.lock();
        this->pos_pixels_over_threshold = msg.data;
        mtx.unlock();
    }
};

int main(int argc, char** argv){
    rclcpp::init(argc, argv);
	rclcpp::NodeOptions options{};
	auto node = std::make_shared<twist_msg_converter>(options);
	rclcpp::spin(node);
	rclcpp::shutdown();
    return 0;
}