#include <iostream>
#include <chrono>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <stdlib.h> 
#include "rclcpp/rclcpp.hpp"
#include <cmath>
#include <math.h>
#include <algorithm>
#include <mutex>
#include <tuple>
#include <sensor_msgs/msg/joy.hpp>
#include <map>
#include <vector>
#include <set>
#include <utility>      // std::pair, std::make_pair
#include <Eigen/LU>
#include <Eigen/QR>
#include "tritonairacer_interfaces/msg/triton_ai_racer_control.hpp"
#include "tritonairacer_interfaces/msg/race_state.hpp"
#include "tritonairacer_interfaces/srv/e_stop.hpp"
#include "tritonairacer_interfaces/srv/change_mode.hpp"
#include "geometry_msgs/msg/vector3.hpp"
#include <string>
#include <fstream>
#include "SLAM_auto_driver/buffer_subscriber.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
using std::placeholders::_1;
std::mutex mtx;

using tritonairacer_interfaces::msg::TritonAIRacerControl;
using tritonairacer_interfaces::srv::EStop;
using tritonairacer_interfaces::msg::RaceState;
using tritonairacer_interfaces::srv::ChangeMode;
using tritonairacer_interfaces::msg::VehicleMode;

using std::placeholders::_1;

#define MAP_SIZE 1500 // will +1 for making origin
#define MAP_RESOLUTION 0.05
#define KP 0.2
#define KD 0.4 // 0.4 is good with 0.2 KP
#define KI 0.0003

double distance(std::pair<double, double>& p1, std::pair<double, double>& p2) {
    return sqrt(pow((p1.first - p2.first),2) + pow((p1.second - p2.second), 2));
}

bool sortBySecond(const std::pair<int, double>& a, const std::pair<int, double>& b) {
    return (a.second < b.second);
}

Eigen::MatrixXd calc_worldTbody(Eigen::MatrixXd& x){
	// x is 3 by 1
	Eigen::MatrixXd worldTbody(4, 4); //4 by n matrix
	worldTbody = Eigen::MatrixXd::Zero(4,4);
	worldTbody(0,0) = 1;
	worldTbody(1,1) = worldTbody(2,2) = worldTbody(3,3) = worldTbody(0,0);
	worldTbody(0,0) = cos(x(2,0));
	worldTbody(0,1) = -sin(x(2,0));
	worldTbody(1,0) = sin(x(2,0));
	worldTbody(1,1) = cos(x(2,0));
	worldTbody(0,3) = x(0,0);
	worldTbody(1,3) = x(1,0);
	return worldTbody;
}

Eigen::MatrixXd calc_T_inv(Eigen::MatrixXd& T){
    Eigen::MatrixXd T_inv(4, 4); //4 by 4 matrix
	T_inv = Eigen::MatrixXd::Zero(4,4);
    Eigen::MatrixXd R = T.block(0, 0, 3, 3);
    Eigen::MatrixXd p = T.block(0, 3, 3, 1);
    Eigen::MatrixXd R_T = R.transpose();
    T_inv(3,3) = 1;
    T_inv.block(0,0,3,3) = R_T;
    T_inv.block(0,3,3,1) = -R_T*p;
return T_inv;
}

class SLAM_auto_driver_Node : public rclcpp::Node{
protected:
    // subscriptions
    rclcpp::Subscription<sensor_msgs::msg::Joy>::SharedPtr joy_subscription_;
    rclcpp::Subscription<geometry_msgs::msg::Vector3>::SharedPtr cur_state_subscription_;
    rclcpp::Subscription<TritonAIRacerControl>::SharedPtr sub_manual_vehicle_cmd_;
    rclcpp::Subscription<VehicleMode>::SharedPtr sub_vehicle_mode_;
    MsgSubscriber<sensor_msgs::msg::LaserScan>::UniquePtr lidar_sub;

    // msg structs
    TritonAIRacerControl SLAM_auto_driver_cmd;
    geometry_msgs::msg::Vector3 cur_car_state;
    TritonAIRacerControl  manual_control_cmd;
    sensor_msgs::msg::LaserScan Lidar_measurement;

    // publishers
    rclcpp::Publisher<TritonAIRacerControl>::SharedPtr SLAM_auto_driver_pub_;

    std::map<std::pair<int,int>, TritonAIRacerControl> gird_commands;
    std::vector<std::pair<geometry_msgs::msg::Vector3,TritonAIRacerControl>> world_coord_commands_vec;
    rclcpp::TimerBase::SharedPtr auto_command_sending_timer_;
    uint32_t auto_command_publisher_freq;
    Eigen::MatrixXd mapTworld = Eigen::MatrixXd::Zero(4,4);
    Eigen::MatrixXd worldTmap = Eigen::MatrixXd::Zero(4,4);
    double prev_cross_track_error = 0.0;
    double cumulation_cross_track_error = 0.0;
    bool first_receive_autonomous = true;
public:
    explicit SLAM_auto_driver_Node(const rclcpp::NodeOptions & options): Node("SLAM_auto_driver_Node", options){
        std::cout << "in SLAM_auto_driver_Node constructor" << std::endl;
        this->joy_subscription_ = this->create_subscription<sensor_msgs::msg::Joy>("/joy", 1, std::bind(&SLAM_auto_driver_Node::topic_callback, this, _1));
		this->cur_state_subscription_ = this->create_subscription<geometry_msgs::msg::Vector3>("/Particle_Filter_SLAM_cur_map_coord", 1, std::bind(&SLAM_auto_driver_Node::receiveState, this, _1));
        this->sub_manual_vehicle_cmd_ = this->create_subscription<TritonAIRacerControl>("manual_vehicle_cmd", 10, std::bind(&SLAM_auto_driver_Node::receiveManual, this, std::placeholders::_1));
        this->SLAM_auto_driver_pub_ = this->create_publisher<TritonAIRacerControl>("autonomous_vehicle_cmd", 1);
        this->sub_vehicle_mode_ = this->create_subscription<VehicleMode>("vehicle_mode", 1, std::bind(&SLAM_auto_driver_Node::receiveMode, this, std::placeholders::_1));
        subscribe_from(this, lidar_sub, "/scan");

        declare_parameter("auto_command_publisher_freq", 10);
        get_parameter("auto_command_publisher_freq", this->auto_command_publisher_freq);

        this->auto_command_sending_timer_ = this->create_wall_timer(std::chrono::milliseconds((uint32_t)(1.0/this->auto_command_publisher_freq*1000)), std::bind(&SLAM_auto_driver_Node::publish_auto_command, this));
        this->SLAM_auto_driver_cmd.steering_openloop.steer = 0.0;
        this->SLAM_auto_driver_cmd.throttle.throttle = 0.0;

        // initialize mapTworld
		mapTworld(0,0) = 1/MAP_RESOLUTION;
		mapTworld(1,1) = 1/MAP_RESOLUTION;
		mapTworld(2,2) = 1;
		mapTworld(3,3) = 1;
		mapTworld(0,3) = MAP_SIZE/2;
		mapTworld(1,3) = MAP_SIZE/2;

        // initialize worldTmap
        Eigen::MatrixXd R = mapTworld.block(0, 0, 3, 3);
        Eigen::MatrixXd p = mapTworld.block(0, 3, 3, 1);
        Eigen::MatrixXd R_T = R.transpose();
        worldTmap(3,3) = 1;
        worldTmap.block(0,0,3,3) = R_T;
        worldTmap.block(0,3,3,1) = -R_T*p;

        // std::ifstream myReadFile;
        // myReadFile.open("trajectory_left_start.txt");
        // if (myReadFile.is_open()) {
        //     while (!myReadFile.eof()) {
        //         double x;
        //         double y;
        //         double theta;
        //         double steering;
        //         double throttle;
        //         myReadFile >> x;
        //         myReadFile >> y;
        //         myReadFile >> theta;
        //         myReadFile >> steering;
        //         myReadFile >> throttle;
        //         //std::cout << x << " " << y << " " << theta << " " << steering << " " << throttle << std::endl;
        //         geometry_msgs::msg::Vector3 state;
        //         state.x = x;
        //         state.y = y;
        //         state.z = theta;
        //         TritonAIRacerControl  control_cmd;
        //         control_cmd.steering_openloop.steer = steering;
        //         control_cmd.throttle.throttle = throttle;
        //         std::pair<geometry_msgs::msg::Vector3,TritonAIRacerControl> aPair(state, control_cmd);
        //         this->world_coord_commands_vec.push_back(aPair);
        //     }
        // }
        // myReadFile.close();

        std::cout << world_coord_commands_vec.size() << std::endl;
    }

    ~SLAM_auto_driver_Node(){
        // save recorded trajectory
        std::ofstream file_out("trajectory.txt", std::ofstream::trunc);
        for(int i = 0; i < world_coord_commands_vec.size(); ++i){
            file_out << world_coord_commands_vec[i].first.x << ' ';
            file_out << world_coord_commands_vec[i].first.y << ' ';
            file_out << world_coord_commands_vec[i].first.z << ' ';
            file_out << world_coord_commands_vec[i].second.steering_openloop.steer << ' ';
            file_out << world_coord_commands_vec[i].second.throttle.throttle << std::endl;
        }
        file_out.close();
    }

    // start_index and finish_index inclusive
    double avg_lidar_rays_measurement(int start_index, int finish_index){
        double sum = 0.0;
        int count = 0;
        for(int i = start_index; i < finish_index + 1; ++i){
            if(!isinf(Lidar_measurement.ranges[i])){
                sum += this->Lidar_measurement.ranges[i];
                count += 1;
            }
        }
        return (sum/count);
    }

    void receiveMode(const VehicleMode msg){
        //std::cout << int(msg.mode == VehicleMode::AUTONOMOUS) << std::endl;
        if(first_receive_autonomous && msg.mode == VehicleMode::AUTONOMOUS){
            this->cumulation_cross_track_error = 0.0;
            first_receive_autonomous = false;
        }
    }

    void receiveState(const geometry_msgs::msg::Vector3 msg) {
        mtx.lock();
        //std::cout << "received state" << std::endl;
        this->cur_car_state = msg;
        mtx.unlock();
    }

    void receiveManual(const TritonAIRacerControl msg) {
		mtx.lock();
        //std::cout << "received manual" << std::endl;
		this->manual_control_cmd = msg;
		mtx.unlock();
	}  

    void topic_callback(const sensor_msgs::msg::Joy::SharedPtr msg) {
        //std::cout << "topic callback function" << std::endl;
        if(msg->buttons[5]){
            mtx.lock();
            //std::cout << "saving manual_control_cmd" << std::endl;
            //std::pair<double,double> car_world_coord(int(cur_car_state.x), int(cur_car_state.y));
            //this->gird_commands[map_coord] = this->manual_control_cmd;
            //std::cout << gird_commands.size() << std::endl;

            this->world_coord_commands_vec.push_back(std::make_pair(this->cur_car_state, this->manual_control_cmd));

            if(world_coord_commands_vec.size() >= 2){
                if(world_coord_commands_vec[world_coord_commands_vec.size()-2].first.x == world_coord_commands_vec[world_coord_commands_vec.size()-1].first.x && world_coord_commands_vec[world_coord_commands_vec.size()-2].first.y == world_coord_commands_vec[world_coord_commands_vec.size()-1].first.y){
                    world_coord_commands_vec.erase(world_coord_commands_vec.begin() + (world_coord_commands_vec.size() - 2));
                }
            }
            std::cout << world_coord_commands_vec.size() << std::endl;
            mtx.unlock();
        }
    }

    void publish_auto_command(){
        //std::cout << "publish auto command" << std::endl;
        mtx.lock();
        std::pair<double,double> car_coord_in_world(double(cur_car_state.x), double(cur_car_state.y));
        Eigen::MatrixXd cur_body_state = Eigen::MatrixXd::Zero(3,1);
        cur_body_state(0,0) = cur_car_state.x; // world x
        cur_body_state(1,0) = cur_car_state.y; // world y
        cur_body_state(2,0) = cur_car_state.z; // world theta

        std::vector<std::pair<int, double>>world_coord_commands_vec_index_distance_list;

        for(int i = 0; i < this->world_coord_commands_vec.size(); ++i){
            std::pair<double,double> trajectory_coord_i(double(world_coord_commands_vec[i].first.x), double(world_coord_commands_vec[i].first.y));
            world_coord_commands_vec_index_distance_list.push_back(std::pair(i,distance(car_coord_in_world, trajectory_coord_i)));
        }

        std::sort(world_coord_commands_vec_index_distance_list.begin(), world_coord_commands_vec_index_distance_list.end(), sortBySecond);

        if(world_coord_commands_vec_index_distance_list.size() != 0){
            int nearest_path_coord_index = world_coord_commands_vec_index_distance_list[0].first;
            TritonAIRacerControl nearest_path_coord_control = world_coord_commands_vec[nearest_path_coord_index].second;
            double distance_to_nearest_world_coord = world_coord_commands_vec_index_distance_list[0].second;
            Eigen::MatrixXd nearest_path_world_coord = Eigen::MatrixXd::Zero(4,1);
            nearest_path_world_coord(3,0) = 1;
            nearest_path_world_coord(0,0) = world_coord_commands_vec[nearest_path_coord_index].first.x;
            nearest_path_world_coord(1,0) = world_coord_commands_vec[nearest_path_coord_index].first.y;

            //std::cout << "nearest_path_world_coord: " << nearest_path_world_coord(0,0) << " " << nearest_path_world_coord(1,0) << std::endl;
            //std::cout << "distance to nearest path coord: " << distance_to_nearest_world_coord << std::endl;

            // while(nearest_path_coord_index != 0){
            //     world_coord_commands_vec.erase(world_coord_commands_vec.begin() + 0);
            // }
            // world_coord_commands_vec.erase(world_coord_commands_vec.begin() + 0);

            Eigen::MatrixXd worldTbody = calc_worldTbody(cur_body_state);
            Eigen::MatrixXd nearest_path_body_coord = calc_T_inv(worldTbody)*nearest_path_world_coord;

            double cross_track_error = 0.0;
            if(nearest_path_body_coord(1,0) < 0){
                cross_track_error = -distance_to_nearest_world_coord;
            }
            else{
                cross_track_error = distance_to_nearest_world_coord;
            }

            double cross_track_error_rate = (cross_track_error - this->prev_cross_track_error)/(1.0/this->auto_command_publisher_freq);
            this->cumulation_cross_track_error += cross_track_error;
            
            double steering_nav_bias = 0.0; // 0.35 is mag max, + is right, - is left
            if(nearest_path_body_coord(1,0) < 0){ // car is on the left of trajectory
                std::cout << "nearest point on car's left with a distance: " << distance_to_nearest_world_coord << std::endl;
                steering_nav_bias = KP*(-distance_to_nearest_world_coord) + KD*cross_track_error_rate + KI*cumulation_cross_track_error;
            }
            else if(nearest_path_body_coord(1,0) > 0){ // car is on the right of trajectory
                std::cout << "nearest point on car's right with a distance: " << distance_to_nearest_world_coord << std::endl;
                steering_nav_bias = KP*(distance_to_nearest_world_coord) + KD*cross_track_error_rate + KI*cumulation_cross_track_error;
            }

            this->prev_cross_track_error = cross_track_error;
            
            this->SLAM_auto_driver_cmd.steering_openloop.steer = nearest_path_coord_control.steering_openloop.steer + steering_nav_bias;
            // this->SLAM_auto_driver_cmd.steering_openloop.steer = steering_nav_bias;
            //this->SLAM_auto_driver_cmd.steering_openloop.steer = nearest_path_coord_control.steering_openloop.steer;
            // this->SLAM_auto_driver_cmd.throttle.throttle = nearest_path_coord_control.throttle.throttle;

            if(this->lidar_sub->has_msg()){
                this->Lidar_measurement = *(this->lidar_sub->take());
                double front_distance = avg_lidar_rays_measurement(225-5, 225+5);
                //std::cout << "front distance: " << front_distance << std::endl;
                if (front_distance < 0.5){
                    this->SLAM_auto_driver_cmd.throttle.throttle = 0.0;
                }
                else{
                    this->SLAM_auto_driver_cmd.throttle.throttle = 10.0;
                }
            }
            //this->SLAM_auto_driver_cmd.throttle.throttle = 8.0;
            
            
            this->SLAM_auto_driver_pub_->publish(this->SLAM_auto_driver_cmd);
        }
        mtx.unlock();
        
    }
};

int main(int argc, char** argv){
	rclcpp::init(argc, argv);
	rclcpp::NodeOptions options{};
	auto node = std::make_shared<SLAM_auto_driver_Node>(options);
	rclcpp::spin(node);
	rclcpp::shutdown();
	return 0;
}