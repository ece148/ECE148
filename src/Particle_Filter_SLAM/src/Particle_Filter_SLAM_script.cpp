#include <iostream>
#include <chrono>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <stdlib.h> 
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/image.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include "sensor_msgs/msg/imu.hpp"
#include "opencv2/core/mat.hpp"
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "Particle_Filter_SLAM/buffer_subscriber.hpp"
#include "Particle_Filter_SLAM/Timer.hpp"
#include <cv_bridge/cv_bridge.h>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <Eigen/LU>
#include <Eigen/QR>
#include <mutex>
#include <tuple>
#include "tritonairacer_interfaces/msg/triton_ai_racer_control.hpp"
#include "tritonairacer_interfaces/msg/race_state.hpp"
#include "tritonairacer_interfaces/srv/e_stop.hpp"
#include "tritonairacer_interfaces/srv/change_mode.hpp"
#include "geometry_msgs/msg/vector3.hpp"
#include <fstream>
#include <random>
#include <time.h>
using std::placeholders::_1;
std::mutex mtx;

#define MAP_SIZE 850 // will +1 for making origin
#define MAP_RESOLUTION 0.05
#define NUM_PARTICLES 1 //175
#define X_VARIANCE 0.0 //0.01
#define Y_VARIANCE 0.0 //0.01
#define THETA_VARIANCE 0.0 //0.0325
#define NEFF_THRESH 140.0 // less than this will trigger resample

void swap(int& x, int& y){
	int temp = y;
	y = x;
	x = temp;
	return;
}

double throttle_to_speed(double throttle){
	return (23250*(throttle/100.0)*0.000257981961);
}

bool sortbythird(const std::tuple<Eigen::MatrixXd, Eigen::MatrixXd, double>& a, const std::tuple<Eigen::MatrixXd, Eigen::MatrixXd, double>& b){
    return (std::get<2>(a) > std::get<2>(b));
}

Eigen::MatrixXd frame_transformation(Eigen::MatrixXd& T, Eigen::MatrixXd& x){
	// T is 4 by 4
	// x is 2 by n
	//std::cout << "T: \n" << T << std::endl;
	//std::cout << "x: \n" << x << std::endl;
	int n = int(x.cols());
	Eigen::MatrixXd homo_vecs(4, n); //4 by n matrix
	homo_vecs = Eigen::MatrixXd::Zero(4,n);
	Eigen::MatrixXd homo_outs(4, n); //4 by n matrix
	homo_outs = Eigen::MatrixXd::Zero(4,n);
	Eigen::MatrixXd ones(1,n);
	ones = Eigen::MatrixXd::Ones(1,n);
	homo_vecs.block(0, 0, 2, n) = x;
	homo_vecs.block(2, 0, 1, n) = Eigen::MatrixXd::Zero(1,n);
	homo_vecs.block(3, 0, 1, n) = ones;
	//std::cout << "homo_vecs: \n" << homo_vecs << std::endl;
	homo_outs = T*homo_vecs;
	//std::cout << "homo_outs: \n" << homo_outs << std::endl;
	return homo_outs;
	//return homo_outs.block(0, 0, 2, n);
}

// <x, y>
void bresenham2d(std::pair<std::pair<double, double>, std::pair<double, double>> theData, std::vector<std::pair<int, int>> *theVector) {
	int y1, x1, y2, x2;
	int dx, dy, sx, sy;
	int e2;
	int error;

	x1 = int(theData.first.first);
	y1 = int(theData.first.second);
	x2 = int(theData.second.first);
	y2 = int(theData.second.second);

	dx = abs(x1 - x2);
	dy = -abs(y1 - y2);

	sx = x1 < x2 ? 1 : -1;
	sy = y1 < y2 ? 1 : -1;

	error = dx + dy;

	while (1) {
		theVector->push_back(std::pair<int, int>(x1, y1));
		if (x1 == x2 && y1 == y2)
			break;
		e2 = 2 * error;

		if (e2 >= dy) {
			if (x2 == x1) break;
			error = error + dy;
			x1 = x1 + sx;
		}
		if (e2 <= dx) {
			if (y2 == y1) break;
			error = error + dx;
			y1 = y1 + sy;
		}
	}
}

Eigen::MatrixXd calc_worldTbody(Eigen::MatrixXd& x){
	// x is 3 by 1
	Eigen::MatrixXd worldTbody(4, 4); //4 by n matrix
	worldTbody = Eigen::MatrixXd::Zero(4,4);
	worldTbody(0,0) = 1;
	worldTbody(1,1) = worldTbody(2,2) = worldTbody(3,3) = worldTbody(0,0);
	worldTbody(0,0) = cos(x(2,0));
	worldTbody(0,1) = -sin(x(2,0));
	worldTbody(1,0) = sin(x(2,0));
	worldTbody(1,1) = cos(x(2,0));
	worldTbody(0,3) = x(0,0);
	worldTbody(1,3) = x(1,0);
	return worldTbody;
}

class ParticleFilterSLAMNode : public rclcpp::Node{
protected:
	// publisher
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr img_pub_;
	rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr trajectory_map_pub_;
	rclcpp::Publisher<geometry_msgs::msg::Vector3>::SharedPtr coord_pub_; // x and y are in integer coord
        
	// timer
	rclcpp::TimerBase::SharedPtr map_sending_timer_;
	rclcpp::TimerBase::SharedPtr step_timer_;
	Timer check_duration_timer;
        
	// parameter
	uint32_t map_publisher_freq;
	double dt_;
	rclcpp::Time last_step_time_;
	bool Lidar_Valid;
	bool firstScan;
	cv::Mat Map = cv::Mat(MAP_SIZE+1, MAP_SIZE+1, CV_32FC1, cv::Scalar(0)); // each grid length 0.1 m
	cv::Mat Trajectory_Map = cv::Mat(MAP_SIZE+1, MAP_SIZE+1, CV_8UC3, cv::Scalar(0, 0, 0)); // each grid length 0.1 m
	double log_odds_map[MAP_SIZE + 1][MAP_SIZE + 1] = {};
	double Map_resolution = MAP_RESOLUTION;
	Eigen::MatrixXd x_t0 = Eigen::MatrixXd::Zero(3,NUM_PARTICLES); // x,y,theta in world frame, initially 0
	Eigen::MatrixXd x_t1 = Eigen::MatrixXd::Zero(3,NUM_PARTICLES); // x,y,theta in world frame, initially 0
	Eigen::MatrixXd mapTworld = Eigen::MatrixXd::Zero(4,4);
	Eigen::MatrixXd bodyTlidar = Eigen::MatrixXd::Zero(4,4);
	Eigen::MatrixXd worldTbody = Eigen::MatrixXd::Zero(4,4);
	rclcpp::Time previous_read_imu_message_time_stamp;
	long long int step_counter;
	double speed = 0.0;
	double angular_velocity_bias = 0.0;
	double angular_velocity = 0.0; // + is clockwise, - is counter clockwise
	geometry_msgs::msg::Vector3 coord_message; 
	std::vector<double> particle_weights;
	Eigen::MatrixXd best_x_t1 = Eigen::MatrixXd::Zero(3,1);

	// msg struct
	tritonairacer_interfaces::msg::TritonAIRacerControl control_cmd;
	sensor_msgs::msg::Imu Imu_measurement;
	sensor_msgs::msg::LaserScan Lidar_measurement;

	// buffer subscriber
	MsgSubscriber<tritonairacer_interfaces::msg::TritonAIRacerControl>::UniquePtr vehicle_control_sub;
	MsgSubscriber<sensor_msgs::msg::Imu>::UniquePtr imu_sub;
	MsgSubscriber<sensor_msgs::msg::LaserScan>::UniquePtr lidar_sub;
public:
	explicit ParticleFilterSLAMNode(const rclcpp::NodeOptions & options): Node("Particle_Filter_SLAM_node", options) {
		std::cout << "in Particle Filter SLAM Node constructor" << std::endl;

		// initialize mapTworld
		mapTworld(0,0) = 1/Map_resolution;
		mapTworld(1,1) = 1/Map_resolution;
		mapTworld(2,2) = 1;
		mapTworld(3,3) = 1;
		mapTworld(0,3) = MAP_SIZE/2;
		mapTworld(1,3) = MAP_SIZE/2;
		std::cout << "mapTworld: \n" << mapTworld << std::endl;

		// initialize bodyTlidar
		bodyTlidar(0,0) = cos(3.1415926535897932384626433832795);
		bodyTlidar(0,1) = -sin(3.1415926535897932384626433832795);
		bodyTlidar(1,0) = sin(3.1415926535897932384626433832795);
		bodyTlidar(1,1) = cos(3.1415926535897932384626433832795);
		bodyTlidar(2,2) = 1;
		bodyTlidar(3,3) = 1;
		std::cout << "bodyTlidar: \n" << bodyTlidar << std::endl;

		// initialize worldTbody
		worldTbody(0,0) = 1;
		worldTbody(1,1) = worldTbody(2,2) = worldTbody(3,3) = worldTbody(0,0);
		std::cout << "worldTbody: \n" << worldTbody << std::endl;

		// initialize coord_message
		coord_message.x = 0;
		coord_message.y = 0;
		coord_message.z = 0;

		// initialize x_t0
		for(int i = 0; i < NUM_PARTICLES; ++i){ // perturbate each particle's x
			unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			std::default_random_engine generator(seed);
			std::normal_distribution<double> distribution(0.0, X_VARIANCE);
			x_t0(0,i) = x_t0(0,i) + distribution(generator);
		}
		for(int i = 0; i < NUM_PARTICLES; ++i){ // perturbate each particle's y
			unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			std::default_random_engine generator(seed);
			std::normal_distribution<double> distribution(0.0, Y_VARIANCE);
			x_t0(1,i) = x_t0(1,i) + distribution(generator);
		}
		for(int i = 0; i < NUM_PARTICLES; ++i){ // perturbate each particle's theta
			unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			std::default_random_engine generator(seed);
			std::normal_distribution<double> distribution(0.0, THETA_VARIANCE);
			x_t0(2,i) = x_t0(2,i) + distribution(generator);
		}
		std::cout << "x_t0: \n" << x_t0 << std::endl;
		this->x_t1 = this->x_t0;

		for(int i = 0; i < NUM_PARTICLES; ++i){
			particle_weights.push_back(1.0/NUM_PARTICLES);
		}

		declare_parameter("map_publisher_freq", 10);
        get_parameter("map_publisher_freq", this->map_publisher_freq);
		declare_parameter("dt", 0.065);
		get_parameter("dt", this->dt_);

		this->firstScan = true;
		std::cout << "angular velocity bias: " << 0.0161 << std::endl;

		// initialize the member publisher
		this->img_pub_ = this->create_publisher<sensor_msgs::msg::Image>("Particle_Filter_SLAM_map", 10);
		this->trajectory_map_pub_ = this->create_publisher<sensor_msgs::msg::Image>("Particle_Filter_SLAM_trajectory_map", 10);
		this->coord_pub_ = this->create_publisher<geometry_msgs::msg::Vector3>("Particle_Filter_SLAM_cur_map_coord", 10);
		
		// initialize the buffer subscribers
		subscribe_from(this, vehicle_control_sub, "/vehicle_cmd");
		subscribe_from(this, imu_sub, "/razor/imu");
		subscribe_from(this, lidar_sub, "/scan");


		// load previous saved oc map
		// this->Map = cv::imread("Map_left_start.png");
		// cv::cvtColor(this->Map, this->Map, CV_BGR2GRAY);
		// this->Map.convertTo(this->Map, CV_32FC1);

		// load previous saved log odds map
		// std::ifstream myReadFile;
        // myReadFile.open("log_odd_map_left_start.txt");
        // if (myReadFile.is_open()) {
        //     for(int i = 0; i < MAP_SIZE + 1; ++i){
		// 		for(int j = 0; j < MAP_SIZE + 1; ++j){
		// 			double map_cell_log_odds;
		// 			myReadFile >> map_cell_log_odds;
		// 			log_odds_map[i][j] = map_cell_log_odds;
		// 		}
        //     }
        // }
        // myReadFile.close();

		// project first lidar measurement, in this case wTb is identity matrix because initially body frame is world frame
		this->map_sending_timer_ = this->create_wall_timer(std::chrono::milliseconds((uint32_t)(1.0/this->map_publisher_freq*1000)), std::bind(&ParticleFilterSLAMNode::publish_map, this));
		// initialize the timer
		this->step_timer_ = rclcpp::create_timer(this, get_clock(), std::chrono::duration<float>(this->dt_), [this] {step();});
	}

	~ParticleFilterSLAMNode(){ // destructor save the occupancy map and log odds map
		cv::imwrite("Map.png", this->Map);
		std::ofstream file_out("log_odd_map.txt", std::ofstream::trunc);
		for(int i = 0; i < MAP_SIZE + 1; ++i){
			for(int j = 0; j < MAP_SIZE + 1; ++j){
				file_out << log_odds_map[i][j] << ' ';
			}
		}	
		file_out.close();
	}

	void publish_map() { // running repeatedly with the timer set frequency
		mtx.lock();
		
		//std::cout << "Particle Filter SLAM publishing map" << std::endl;
		// OpenCV example (matplotlib equivalent)
		// cv::Mat single_channel_image(5, 5, CV_32FC1, cv::Scalar(0));
		// float* single_channel_pixel_ptr = (float*)single_channel_image.data;
		// single_channel_pixel_ptr[2*single_channel_image.cols + 2] = 0.8;
		// single_channel_pixel_ptr[0*single_channel_image.cols + 0] = 0.6;
		// cv::threshold(single_channel_image, single_channel_image, 0.5, 255.0, 0);
		cv::Mat single_channel_image = this->Map;
		single_channel_image.convertTo(single_channel_image, CV_8UC1);

		// example of publishing image out
		// Avoid copying image message if possible
		sensor_msgs::msg::Image::UniquePtr image_msg(new sensor_msgs::msg::Image());
		auto stamp = now();
		// Convert OpenCV Mat to ROS Image
		image_msg->header.stamp = stamp;
		// image_msg->header.frame_id = cxt_.camera_frame_id_;
		image_msg->height = single_channel_image.rows;
		image_msg->width = single_channel_image.cols;
		image_msg->encoding = "8UC1";
		image_msg->is_bigendian = false;
		image_msg->step = static_cast<sensor_msgs::msg::Image::_step_type>(single_channel_image.step);
		image_msg->data.assign(single_channel_image.datastart,single_channel_image.dataend);
		img_pub_->publish(std::move(image_msg));

		//this->Trajectory_Map; // 8UC3
		sensor_msgs::msg::Image::UniquePtr trajectory_map_msg(new sensor_msgs::msg::Image());
		stamp = now();
		trajectory_map_msg->header.stamp = stamp;
		trajectory_map_msg->height = this->Trajectory_Map.rows;
		trajectory_map_msg->width = this->Trajectory_Map.cols;
		trajectory_map_msg->encoding = "8UC3";
		trajectory_map_msg->is_bigendian = false;
		trajectory_map_msg->step = static_cast<sensor_msgs::msg::Image::_step_type>(this->Trajectory_Map.step);
		trajectory_map_msg->data.assign(this->Trajectory_Map.datastart,this->Trajectory_Map.dataend);
		trajectory_map_pub_->publish(std::move(trajectory_map_msg));


		// publish x and y coord in map
		coord_pub_->publish(this->coord_message);
		mtx.unlock();
    }

	void predictionStep(){
		this->speed = 0.0;
		this->angular_velocity_bias = 0.0;
		this->angular_velocity = 0.0; // + is clockwise, - is counter clockwise
		this->Imu_measurement = *(this->imu_sub->take());
		//std::cout << Imu_measurement.header.stamp.nsec << std::endl;
		this->control_cmd = *(this->vehicle_control_sub->take());
		this->speed = throttle_to_speed(this->control_cmd.throttle.throttle);
		if(this->speed){
			this->angular_velocity_bias = 0.0179; // tuned
		}
		if(!(abs(this->Imu_measurement.angular_velocity.z) <= 0.02)){
			this->angular_velocity = this->Imu_measurement.angular_velocity.z;
		}

		this->x_t0 = this->x_t1;

		for(int i = 0; i < NUM_PARTICLES; ++i){
			this->x_t1(0,i) = x_t0(0,i) + this->dt_*this->speed*cos(x_t0(2,i));
			this->x_t1(1,i) = x_t0(1,i) + this->dt_*this->speed*sin(x_t0(2,i));
			this->x_t1(2,i) = x_t0(2,i) + this->dt_*(this->angular_velocity + this->angular_velocity_bias);
		}
		return;
	}

	Eigen::MatrixXd filterValidLidar(){
			this->Lidar_measurement = *(this->lidar_sub->take());
			std::vector<std::pair<double, double>> Valid_Lidar_measurement; // in lidar frame x, y
			for (int i=0; i<450; ++i){
				this->Lidar_Valid = Lidar_measurement.ranges[i]>0.1 && Lidar_measurement.ranges[i]<7;
				if(Lidar_Valid){
					double angle = 2*M_PI-i*Lidar_measurement.angle_increment;
					Valid_Lidar_measurement.push_back(std::make_pair(Lidar_measurement.ranges[i]*cos(angle), Lidar_measurement.ranges[i]*sin(angle))); // lidar frame cord: x, y 
					//std::cout <<"Valid_Lidar_measurement:" << Valid_Lidar_measurement[Valid_Lidar_measurement.size()-1].first << " " <<Valid_Lidar_measurement[Valid_Lidar_measurement.size()-1].second<< std::endl;
				}
			}
			int n = Valid_Lidar_measurement.size();
			Eigen::MatrixXd valid_lidar_coords_lidar_frame = Eigen::MatrixXd::Zero(2,n);
			for(int i = 0; i < n; ++i){
				valid_lidar_coords_lidar_frame(0,i) = Valid_Lidar_measurement[i].first;
				valid_lidar_coords_lidar_frame(1,i) = Valid_Lidar_measurement[i].second; 
			}
			return valid_lidar_coords_lidar_frame;
	}

	double minimum_val(double a, double b){
		if(a > b)
			return b;
		return a;
	}

	double maximum_val(double a, double b){
		if(a < b)
			return b;
		return a;
	}

	void updateLogOdds(Eigen::MatrixXd& lidarPoints){
		std::pair<std::pair<double, double>, std::pair<double, double>> toBresen;
		std::vector<std::pair<int, int>> emptyCells;
		Eigen::MatrixXd x_coord = Eigen::MatrixXd::Zero(2,1);
		int NumOfPoints = lidarPoints.cols();
		x_coord(0,0) = x_t1(0,0);
		x_coord(1,0) = x_t1(1,0);
		Eigen::MatrixXd x_map_frame = frame_transformation(this->mapTworld,x_coord);
		toBresen.first.first = int(round(x_map_frame(0,0)));
		toBresen.first.second = int(round(x_map_frame(1,0)));

		for(int i = 0; i < NumOfPoints; ++i){
			toBresen.second.first = lidarPoints(0,i);
			toBresen.second.second = lidarPoints(1,i);
			log_odds_map[int(round(lidarPoints(0,i)))][int(round(lidarPoints(1,i)))] = 
			minimum_val(log_odds_map[int(round(lidarPoints(0,i)))][int(round(lidarPoints(1,i)))] + 5*std::log(8), 1000*std::log(8));
			bresenham2d(toBresen,&emptyCells);
		}
		for(auto &it: emptyCells){
			log_odds_map[it.first][it.second] = maximum_val(-std::log(8)+log_odds_map[it.first][it.second], -300*std::log(8));
		}
		float* Map_pixel_ptr = (float*)Map.data;
		for(int i = 0; i < MAP_SIZE + 1; ++i)
			for(int j = 0; j < MAP_SIZE + 1; ++j)
					if(log_odds_map[i][j]>0){
						Map_pixel_ptr[j*Map.cols + i] = 255.0;
					}
					else{
						Map_pixel_ptr[j*Map.cols + i] = 0;
					}
	}

	void step(){
		this->check_duration_timer.reset();
		mtx.lock();
		// std::cout << "in step" << std::endl;
		if(this->firstScan){ // project first lidar data to the origin of the map
			if(this->lidar_sub->has_msg()){ 
				Eigen::MatrixXd valid_lidar_coords_lidar_frame = filterValidLidar();
				Eigen::MatrixXd valid_lidar_coords_body_frame = frame_transformation(this->bodyTlidar,valid_lidar_coords_lidar_frame);
				Eigen::MatrixXd valid_lidar_coords_world_frame = this->worldTbody*valid_lidar_coords_body_frame;
				Eigen::MatrixXd valid_lidar_coords_map_frame = this->mapTworld*valid_lidar_coords_world_frame;
				updateLogOdds(valid_lidar_coords_map_frame);
				this->firstScan = false;
			}
		}
		else{ // first lidar scan already projected to origin of the map, prediction and update can begin
			if(this->imu_sub->has_msg() && this->vehicle_control_sub->has_msg()){
				predictionStep();
			}
			else{
				std::cout << "no prediction step" << std::endl;
			}
			if(this->lidar_sub->has_msg()){ 
				Eigen::MatrixXd valid_lidar_coords_lidar_frame = filterValidLidar();
				Eigen::MatrixXd valid_lidar_coords_body_frame = frame_transformation(this->bodyTlidar,valid_lidar_coords_lidar_frame);

				float* Map_pixel_ptr = (float*)Map.data;

				std::vector<std::tuple<Eigen::MatrixXd, Eigen::MatrixXd, double>> x_t1_candidate_score_list; // x_t1, valid_lidar_coords_map_frame, score
				double score_sum = 0.0;
				for(int i = 0; i < NUM_PARTICLES; ++i){
					Eigen::MatrixXd x_t1_candidate = x_t1.block(0,i,3,1);
					Eigen::MatrixXd worldTbody_candidate = calc_worldTbody(x_t1_candidate);
					double score;
					Eigen::MatrixXd valid_lidar_coords_world_frame_candidate = worldTbody_candidate*valid_lidar_coords_body_frame;
					Eigen::MatrixXd valid_lidar_coords_map_frame_candidate = this->mapTworld*valid_lidar_coords_world_frame_candidate;
					// find map correlation
					for(int i = 0; i < valid_lidar_coords_map_frame_candidate.cols(); ++i){
						score += Map_pixel_ptr[int(round(valid_lidar_coords_map_frame_candidate(1,i)))*Map.cols + int(round(valid_lidar_coords_map_frame_candidate(0,i)))];
					}
					score_sum += score;
					x_t1_candidate_score_list.push_back(std::make_tuple(x_t1_candidate, valid_lidar_coords_map_frame_candidate, score));
				}

				for(int i = 0; i < NUM_PARTICLES; ++i){ // update weights
					this->particle_weights[i] = std::get<2>(x_t1_candidate_score_list[i])/score_sum;
				}

				std::sort(x_t1_candidate_score_list.begin(), x_t1_candidate_score_list.end(), sortbythird);
				this->best_x_t1 = std::get<0>(x_t1_candidate_score_list[0]);
				updateLogOdds(std::get<1>(x_t1_candidate_score_list[0]));

				double Neff_denominator = 0.0;
				double Neff;
				for(int i = 0; i < NUM_PARTICLES; ++i){
					Neff_denominator += pow(particle_weights[i],2);
				}
				Neff = 1.0/Neff_denominator;
				std::cout << "Neff: " << Neff << std::endl;
				if(Neff < NEFF_THRESH){
					this->resample_particles();
				}
			}
			else{
				std::cout << "no update step" << std::endl;
			}
		}

		Eigen::MatrixXd x_coord = Eigen::MatrixXd::Zero(2,1);
		x_coord(0,0) = best_x_t1(0,0);
		x_coord(1,0) = best_x_t1(1,0);
		Eigen::MatrixXd x_map_frame = frame_transformation(this->mapTworld, x_coord);
		uint8_t* Trajectory_Map_pixel_ptr = (uint8_t*)this->Trajectory_Map.data;
		Trajectory_Map_pixel_ptr[int(round(x_map_frame(1,0)))*Trajectory_Map.cols*3 + int(round(x_map_frame(0,0)))*3+0] = 255; // plots trajectory
		Trajectory_Map_pixel_ptr[int(round(x_map_frame(1,0)))*Trajectory_Map.cols*3 + int(round(x_map_frame(0,0)))*3+1] = 255; // plots trajectory
		Trajectory_Map_pixel_ptr[int(round(x_map_frame(1,0)))*Trajectory_Map.cols*3 + int(round(x_map_frame(0,0)))*3+2] = 255; // plots trajectory
		
		this->coord_message.x = best_x_t1(0,0);
		this->coord_message.y = best_x_t1(1,0);
		this->coord_message.z = best_x_t1(2,0);	
		

		mtx.unlock();
		std::cout << "step duration: " << this->check_duration_timer.elapsed() << std::endl;
		return;
	}

	void resample_particles(){
		double resampled_count = 0.0;
		for(int i = 0; i < particle_weights.size(); ++i){
			if(particle_weights[i] < (1.0/NUM_PARTICLES)/2.0){
				resampled_count += 1.0;
				unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
				std::default_random_engine generator1(seed1);
				std::normal_distribution<double> distribution1(0.0, X_VARIANCE);
				x_t1(0,i) = this->best_x_t1(0,0) + distribution1(generator1);

				unsigned seed2 = std::chrono::system_clock::now().time_since_epoch().count();
				std::default_random_engine generator2(seed2);
				std::normal_distribution<double> distribution2(0.0, Y_VARIANCE);
				x_t1(1,i) = this->best_x_t1(1,0) + distribution2(generator2);

				unsigned seed3 = std::chrono::system_clock::now().time_since_epoch().count();
				std::default_random_engine generator3(seed3);
				std::normal_distribution<double> distribution3(0.0, THETA_VARIANCE);
				x_t1(2,i) = this->best_x_t1(2,0) + distribution3(generator3);
			}
		}
		double portion_resampled = resampled_count/NUM_PARTICLES;
		std::cout << "portion of particles got resampled: " << portion_resampled << std::endl;
	}
};

int main(int argc, char** argv){
	// Eigen(linear algebra) example
	Eigen::MatrixXd T(4,4); // zero initialized matrix
	T = Eigen::MatrixXd::Zero(4,4);
	T(0,0) = 2;
	T(2,2) = T(1,1) = T(0,0);
	T(3,3) = 1;
	std::cout << "T: \n" << T << std::endl;

	// Eigen::MatrixXd x(4,1); // zero initialized vector
	// std::cout << x << std::endl;
	// x(0,0) = 1;
	// x(1,0) = 2;
	// x(2,0) = 0;
	// x(3,0) = 1;
	// std::cout << x << std::endl;

	// std::cout << T * x << std::endl;

	Eigen::MatrixXd x(2,3);
	x = Eigen::MatrixXd::Zero(2,3);
	x(0,0) = 1;
	x(1,0) = 2;
	x(0,1) = 3.5;
	x(1,1) = 4.3;
	x(0,2) = 8.8;
	x(1,2) = 9.9;
	std::cout << "x: \n" << x << std::endl;

	std::cout << "result: \n" << frame_transformation(T,x) << std::endl;

	// while(true){
	// 	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	// 	std::default_random_engine generator(seed);
	// 	std::normal_distribution<double> distribution(0.0, 0.0);
	// 	std::cout << distribution(generator) << std::endl;
	// }

	rclcpp::init(argc, argv);
	rclcpp::NodeOptions options{};
	auto node = std::make_shared<ParticleFilterSLAMNode>(options);
	rclcpp::spin(node);
	rclcpp::shutdown();
	return 0;
}
