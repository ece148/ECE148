#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <math.h>
#include <utility>
#include <vector>
#include <iostream>
#include <thread>
#include <mutex>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int16.hpp"
#include "std_msgs/msg/u_int16.hpp"
#include "std_msgs/msg/string.hpp"

#include "tritonairacer_interfaces/msg/triton_ai_racer_control.hpp"
#include "tritonairacer_interfaces/msg/race_state.hpp"
#include "tritonairacer_interfaces/srv/e_stop.hpp"
#include "tritonairacer_interfaces/srv/change_mode.hpp"

std::mutex mtx;

using namespace std::placeholders;
using tritonairacer_interfaces::msg::TritonAIRacerControl;
using tritonairacer_interfaces::srv::EStop;
using tritonairacer_interfaces::msg::RaceState;
using tritonairacer_interfaces::srv::ChangeMode;
using tritonairacer_interfaces::msg::VehicleMode;

using namespace std::placeholders;

class Pilot : public rclcpp::Node {
  private:
	rclcpp::Publisher<TritonAIRacerControl>::SharedPtr pub_control_;

	rclcpp::Subscription<RaceState>::SharedPtr sub_race_state_;
	rclcpp::Subscription<TritonAIRacerControl>::SharedPtr sub_manual_vehicle_cmd_;
	rclcpp::Subscription<TritonAIRacerControl>::SharedPtr sub_autonomous_vehicle_cmd_;

	rclcpp::Service<EStop>::SharedPtr srv_estop_;
	rclcpp::Service<ChangeMode>::SharedPtr srv_change_mode_;

	rclcpp::TimerBase::SharedPtr control_timer_;

	uint32_t control_publisher_freq;
	VehicleMode vehicle_mode;
  public:
	std_msgs::msg::UInt16 throttle_cmd;
    	std_msgs::msg::UInt16 brake_cmd;
    	std_msgs::msg::Int16 steer_cmd;

	TritonAIRacerControl  control_cmd;

	Pilot() : rclcpp::Node("pilot") {
		this->throttle_cmd.data = 0;
		this->brake_cmd.data = 0;
		this->steer_cmd.data = 0;
		declare_parameter("control_publisher_freq", 20);
		get_parameter("control_publisher_freq", control_publisher_freq);

		// Publishers 
		this->pub_control_ = create_publisher<TritonAIRacerControl>("vehicle_cmd", 1);
		
		// Subscribers
		this->sub_race_state_ = this->create_subscription<RaceState>("/rcs_state", 10, std::bind(&Pilot::receiveRCSState, this, std::placeholders::_1));
		this->sub_manual_vehicle_cmd_ = this->create_subscription<TritonAIRacerControl>("manual_vehicle_cmd", 10, std::bind(&Pilot::receiveManual, this, std::placeholders::_1));
		this->sub_autonomous_vehicle_cmd_ = this->create_subscription<TritonAIRacerControl>("autonomous_vehicle_cmd", 10, std::bind(&Pilot::receiveAutonomous, this, std::placeholders::_1));

		//Service 
		this->srv_estop_ = create_service<EStop>("EStop_service", std::bind(&Pilot::service_estop, this, std::placeholders::_1, std::placeholders::_2));
		this->srv_change_mode_ = create_service<ChangeMode>("ChangeMode_service", std::bind(&Pilot::service_change_mode, this, std::placeholders::_1, std::placeholders::_2));
		// Callback timers
		this->control_timer_ = this->create_wall_timer(std::chrono::milliseconds((uint32_t)(1.0 / control_publisher_freq * 1000)), std::bind(&Pilot::send_control_callback, this));

	}
	// Pilot(Pilot &p) : rclcpp::Node("pilot") {}
	// ~Pilot() {}

	void service_estop(const std::shared_ptr<EStop::Request> request, std::shared_ptr<EStop::Response> response){
		response->estop_engaged = false;
		std::cout << "pilot received base station request estop" << std::endl;
	}

	void service_change_mode(const std::shared_ptr<ChangeMode::Request> request, std::shared_ptr<ChangeMode::Response> response){
		mtx.lock();
		this->vehicle_mode.mode = request->mode_request.mode;
		response->mode_response.mode = this->vehicle_mode.mode;
		std::cout << "pilot recieved MANUAL request: " << (this->vehicle_mode.mode == VehicleMode::MANUAL) << std::endl;
		std::cout << "pilot recieved AUTONOMOUS request: " << (this->vehicle_mode.mode == VehicleMode::AUTONOMOUS) << std::endl;
		std::cout << "pilot recieved ESTOP request: " << (this->vehicle_mode.mode == VehicleMode::EMERGENCY_STOP) << std::endl;
		if(this->vehicle_mode.mode == VehicleMode::EMERGENCY_STOP) {
			std::cout << "call to emergency stop" << std::endl;
			mtx.unlock();
			this->emergencyStop();
			mtx.lock();
		}
		mtx.unlock();
	}
	
	void emergencyStop() {
		std::lock_guard<std::mutex> lock(mtx);
		this->vehicle_mode.mode = VehicleMode::EMERGENCY_STOP; // this will stop updating vehicle_cmd from listening to manual/autonomous topics
		this->control_cmd.brake.brake = 0;
		this->control_cmd.throttle.throttle = 0;
		std::cout << "emergency brake update data" << std::endl;
	}

	void receiveRCSState(const RaceState msg) {
		std::cout << "rcs stage recieved: " << msg.stage.stage << std::endl;
		std::cout << "rcs flag recieved: " << msg.flag.race_flag << std::endl;

		if(msg.flag.race_flag == msg.flag.EMERGENCY_STOP) {
			// RED_RED
			this->emergencyStop(); // this will set member vehicle to 0 throttle and 100 brake
			std::cout << "rcs: RED_RED" << std::endl;
		}
		else if(msg.flag.race_flag == msg.flag.RED) {
			// RED_FLAG
			std::cout << "rcs: RED_FLAG" << std::endl;
		}
		else if(msg.stage.stage == msg.stage.IN_GARAGE) {
			// IN_GARAGE
			std::cout << "rcs: IN_GARAGE" << std::endl;
		}
		else if(msg.stage.stage == msg.stage.ON_GRID) {
			// GRID ACTIVE
			std::cout << "rcs: GRID_ACTIVE" << std::endl;
		}
		else if(msg.stage.stage == msg.stage.IN_RACE) {
			// GREEN_GREEN
			std::cout << "rcs: GREEN_GREEN" << std::endl;
		}
		
	}

	void receiveManual(const TritonAIRacerControl msg) {
		if(this->vehicle_mode.mode != VehicleMode::MANUAL) return;
		mtx.lock();
		this->control_cmd = msg;
		mtx.unlock();
	}  

	void receiveAutonomous(const TritonAIRacerControl msg) {
		if(this->vehicle_mode.mode != VehicleMode::AUTONOMOUS) return;
		mtx.lock();
		this->control_cmd = msg;
		mtx.unlock();
	}


	void send_control_callback() { // running repeatedly with the timer set frequency
		mtx.lock();
		this->pub_control_->publish(this->control_cmd);
		mtx.unlock();
	}

	void print_local(){ // logging purpose	
		std::cout << "throttle: " << this->throttle_cmd.data << std::endl;
		std::cout << "brake: " << this->brake_cmd.data << std::endl;
		std::cout << "steer: " << this->steer_cmd.data << std::endl;
	}
};

int main(int argc, char * argv[]){
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<Pilot>());
  rclcpp::shutdown();
  return 0;
}

