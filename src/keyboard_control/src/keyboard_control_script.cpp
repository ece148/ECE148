#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <math.h>
#include <utility>
#include <vector>
#include <iostream>
#include <thread>
#include <mutex>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int16.hpp"
#include "std_msgs/msg/u_int16.hpp"
#include "std_msgs/msg/string.hpp"

#include "tritonairacer_interfaces/msg/triton_ai_racer_control.hpp"
#include "tritonairacer_interfaces/msg/race_state.hpp"
#include "tritonairacer_interfaces/srv/e_stop.hpp"
#include "tritonairacer_interfaces/srv/change_mode.hpp"

#include <signal.h>
#include <stdio.h>
#ifdef _WIN32
# include <windows.h>
#else
# include <termios.h>
# include <unistd.h>
#endif

std::mutex mtx;

using namespace std::placeholders;
using tritonairacer_interfaces::msg::TritonAIRacerControl;
using tritonairacer_interfaces::srv::EStop;
using tritonairacer_interfaces::msg::RaceState;
using tritonairacer_interfaces::srv::ChangeMode;
using tritonairacer_interfaces::msg::VehicleMode;

using namespace std::placeholders;

static constexpr char KEYCODE_RIGHT = 0x43;
static constexpr char KEYCODE_LEFT = 0x44;
static constexpr char KEYCODE_UP = 0x41;
static constexpr char KEYCODE_DOWN = 0x42;
static constexpr char KEYCODE_B = 0x62;
static constexpr char KEYCODE_C = 0x63;
static constexpr char KEYCODE_D = 0x64;
static constexpr char KEYCODE_E = 0x65;
static constexpr char KEYCODE_F = 0x66;
static constexpr char KEYCODE_G = 0x67;
static constexpr char KEYCODE_Q = 0x71;
static constexpr char KEYCODE_R = 0x72;
static constexpr char KEYCODE_T = 0x74;
static constexpr char KEYCODE_V = 0x76;

bool running = true;

class KeyboardReader final
{
public:
  KeyboardReader()
  {
#ifdef _WIN32
    hstdin_ = GetStdHandle(STD_INPUT_HANDLE);
    if (hstdin_ == INVALID_HANDLE_VALUE)
    {
      throw std::runtime_error("Failed to get stdin handle");
    }
    if (!GetConsoleMode(hstdin_, &old_mode_))
    {
      throw std::runtime_error("Failed to get old console mode");
    }
    DWORD new_mode = ENABLE_PROCESSED_INPUT;  // for Ctrl-C processing
    if (!SetConsoleMode(hstdin_, new_mode))
    {
      throw std::runtime_error("Failed to set new console mode");
    }
#else
    // get the console in raw mode
    if (tcgetattr(0, &cooked_) < 0)
    {
      throw std::runtime_error("Failed to get old console mode");
    }
    struct termios raw;
    memcpy(&raw, &cooked_, sizeof(struct termios));
    raw.c_lflag &=~ (ICANON | ECHO);
    // Setting a new line, then end of file
    raw.c_cc[VEOL] = 1;
    raw.c_cc[VEOF] = 2;
    raw.c_cc[VTIME] = 1;
    raw.c_cc[VMIN] = 0;
    if (tcsetattr(0, TCSANOW, &raw) < 0)
    {
      throw std::runtime_error("Failed to set new console mode");
    }
#endif
  }

  char readOne()
  {
    char c = 0;

#ifdef _WIN32
    INPUT_RECORD record;
    DWORD num_read;
    switch (WaitForSingleObject(hstdin_, 100))
    {
    case WAIT_OBJECT_0:
      if (!ReadConsoleInput(hstdin_, &record, 1, &num_read))
      {
        throw std::runtime_error("Read failed");
      }

      if (record.EventType != KEY_EVENT || !record.Event.KeyEvent.bKeyDown) {
        break;
      }

      if (record.Event.KeyEvent.wVirtualKeyCode == VK_LEFT)
      {
        c = KEYCODE_LEFT;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == VK_UP)
      {
        c = KEYCODE_UP;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == VK_RIGHT)
      {
        c = KEYCODE_RIGHT;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == VK_DOWN)
      {
        c = KEYCODE_DOWN;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == 0x42)
      {
        c = KEYCODE_B;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == 0x43)
      {
        c = KEYCODE_C;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == 0x44)
      {
        c = KEYCODE_D;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == 0x45)
      {
        c = KEYCODE_E;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == 0x46)
      {
        c = KEYCODE_F;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == 0x47)
      {
        c = KEYCODE_G;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == 0x51)
      {
        c = KEYCODE_Q;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == 0x52)
      {
        c = KEYCODE_R;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == 0x54)
      {
        c = KEYCODE_T;
      }
      else if (record.Event.KeyEvent.wVirtualKeyCode == 0x56)
      {
        c = KEYCODE_V;
      }
      break;

    case WAIT_TIMEOUT:
      break;
    }

#else
    int rc = read(0, &c, 1);
    if (rc < 0)
    {
      throw std::runtime_error("read failed");
    }
#endif

    return c;
  }

  ~KeyboardReader()
  {
#ifdef _WIN32
    SetConsoleMode(hstdin_, old_mode_);
#else
    tcsetattr(0, TCSANOW, &cooked_);
#endif
  }

private:
#ifdef _WIN32
  HANDLE hstdin_;
  DWORD old_mode_;
#else
  struct termios cooked_;
#endif
};

class Keyboard_Control final
{
public:
  Keyboard_Control()
  {
    nh_ = rclcpp::Node::make_shared("keyboard_control");
    //nh_->declare_parameter("scale_angular", 2.0);
    //nh_->declare_parameter("scale_linear", 2.0);

    keyboard_pub_ = nh_->create_publisher<TritonAIRacerControl>("manual_vehicle_cmd", 1);
    this->keyboard_control_cmd.brake.brake = 0;
    this->keyboard_control_cmd.throttle.throttle = 0;
    this->keyboard_control_cmd.steering_openloop.steer = 0.0;
    this->keyboard_control_cmd.steering_rad.steer = 0.0;
  }

  int keyLoop()
  {
    char c;

    std::thread{std::bind(&Keyboard_Control::spin, this)}.detach();

    puts("Reading from keyboard");
    puts("---------------------------");
    puts("Use arrow keys to move the car.");
    puts("ONLY PRESS ONE KEY AT THE TIME!!!!!");
    puts("'Q' to quit.");

    while (running)
    {
      // get the next event from the keyboard
      try
      {
        c = input_.readOne();
      }
      catch (const std::runtime_error &)
      {
        perror("read():");
        return -1;
      }

      //double linear = 0.0;
      //double angular = 0.0;

      RCLCPP_DEBUG(nh_->get_logger(), "value: 0x%02X\n", c);

      switch(c)
      {
      case KEYCODE_LEFT:
        RCLCPP_DEBUG(nh_->get_logger(), "LEFT");
        //angular = 1.0;
	//std::cout << "left" << std::endl;
	if(keyboard_control_cmd.steering_openloop.steer != -0.25){
		this->keyboard_control_cmd.steering_openloop.steer -= .25;
	}
	if(keyboard_control_cmd.steering_rad.steer < 0.0025 && keyboard_control_cmd.steering_rad.steer > -0.0025){
		this->keyboard_control_cmd.steering_rad.steer -= 0.785;
	}
	if(keyboard_control_cmd.steering_rad.steer < 0.785+ .0025 && keyboard_control_cmd.steering_rad.steer > 0.785 - .0025){
		this->keyboard_control_cmd.steering_rad.steer -= 0.785;
	}
        break;
      case KEYCODE_RIGHT:
        RCLCPP_DEBUG(nh_->get_logger(), "RIGHT");
        //angular = -1.0;
	//std::cout << "right" << std::endl;
	if(keyboard_control_cmd.steering_openloop.steer != 0.25){
		this->keyboard_control_cmd.steering_openloop.steer += .25;
	}
	if(keyboard_control_cmd.steering_rad.steer < 0.0025 && keyboard_control_cmd.steering_rad.steer > -0.0025){
		this->keyboard_control_cmd.steering_rad.steer += 0.785;
	}
	if(keyboard_control_cmd.steering_rad.steer > -0.785 - .0025 && keyboard_control_cmd.steering_rad.steer < -0.785 + .0025){
		this->keyboard_control_cmd.steering_rad.steer += 0.785;
	}
        break;
      case KEYCODE_UP:
        RCLCPP_DEBUG(nh_->get_logger(), "UP");
        //linear = 1.0;
	//std::cout << "up" << std::endl;
	if(keyboard_control_cmd.throttle.throttle != 20 || keyboard_control_cmd.brake.brake == 20 && keyboard_control_cmd.throttle.throttle == 0){		
		if(keyboard_control_cmd.brake.brake == 0 && keyboard_control_cmd.throttle.throttle == 0){
			this->keyboard_control_cmd.throttle.throttle += 20;
		}
		if(keyboard_control_cmd.brake.brake == 30 && keyboard_control_cmd.throttle.throttle == 0){
			//this->keyboard_control_cmd.throttle.throttle += 30;
			this->keyboard_control_cmd.brake.brake -= 20;
		}
	}
        break;
      case KEYCODE_DOWN:
        RCLCPP_DEBUG(nh_->get_logger(), "DOWN");
        //linear = -1.0;
	//std::cout << "down" << std::endl;
	if(keyboard_control_cmd.brake.brake != 20 || keyboard_control_cmd.throttle.throttle == 20 && keyboard_control_cmd.brake.brake == 0) {
		if(keyboard_control_cmd.brake.brake == 0 && keyboard_control_cmd.throttle.throttle == 0){
			this->keyboard_control_cmd.brake.brake += 20;
		}
		if(keyboard_control_cmd.brake.brake == 0 && keyboard_control_cmd.throttle.throttle == 20){
			this->keyboard_control_cmd.throttle.throttle -= 20;
			//this->keyboard_control_cmd.brake.brake += 30;
		}
	}
        break;
      case KEYCODE_G:
        RCLCPP_DEBUG(nh_->get_logger(), "G");
        break;
      case KEYCODE_T:
        RCLCPP_DEBUG(nh_->get_logger(), "T");
        break;
      case KEYCODE_R:
        RCLCPP_DEBUG(nh_->get_logger(), "R");
        break;
      case KEYCODE_E:
        RCLCPP_DEBUG(nh_->get_logger(), "E");
        break;
      case KEYCODE_D:
        RCLCPP_DEBUG(nh_->get_logger(), "D");
        break;
      case KEYCODE_C:
        RCLCPP_DEBUG(nh_->get_logger(), "C");
        break;
      case KEYCODE_V:
        RCLCPP_DEBUG(nh_->get_logger(), "V");
        break;
      case KEYCODE_B:
        RCLCPP_DEBUG(nh_->get_logger(), "B");
        break;
      case KEYCODE_F:
        RCLCPP_DEBUG(nh_->get_logger(), "F");
        break;
      case KEYCODE_Q:
        RCLCPP_DEBUG(nh_->get_logger(), "quit");
        running = false;
        break;
      default:
        // This can happen if the read returned when there was no data, or
        // another key was pressed.  In these cases, just silently ignore the
        // press.
	//std::cout << "nothing is pressed" << std::endl;
        break;
      }

      if (running)
      {
        //geometry_msgs::msg::Twist twist;
        //twist.angular.z = nh_->get_parameter("scale_angular").as_double() * angular;
        //twist.linear.x = nh_->get_parameter("scale_linear").as_double() * linear;
        this->keyboard_pub_->publish(this->keyboard_control_cmd);
      }
    }

    return 0;
  }

private:
  void spin()
  {
    rclcpp::spin(nh_);
  }

  rclcpp::Node::SharedPtr nh_;
  rclcpp::Publisher<TritonAIRacerControl>::SharedPtr keyboard_pub_;
  //rclcpp_action::Client<turtlesim::action::RotateAbsolute>::SharedPtr rotate_absolute_client_;
  //rclcpp_action::ClientGoalHandle<turtlesim::action::RotateAbsolute>::SharedPtr goal_handle_;

  KeyboardReader input_;
  TritonAIRacerControl keyboard_control_cmd;
};

#ifdef _WIN32
BOOL WINAPI quit(DWORD ctrl_type)
{
  (void)ctrl_type;
  running = false;
  return true;
}
#else
void quit(int sig)
{
  (void)sig;
  running = false;
}
#endif

int main(int argc, char** argv)
{
  rclcpp::init(argc, argv);

#ifdef _WIN32
  SetConsoleCtrlHandler(quit, TRUE);
#else
  signal(SIGINT, quit);
#endif

  Keyboard_Control keyboard_control;

  int rc = keyboard_control.keyLoop();

  rclcpp::shutdown();

  return rc;
}

