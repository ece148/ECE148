import rclpy
import numpy as np
from rclpy.node import Node
import matplotlib.pyplot as plt
import pickle

from std_msgs.msg import String
from sensor_msgs.msg import Image
from rclpy.qos import qos_profile_sensor_data
from tritonairacer_interfaces.msg import TritonAIRacerControl

import threading

lock = threading.Lock()

class Data_Saver(Node):
    def __init__(self):
        super().__init__('minimal_subscriber')
        self.image_subscription = self.create_subscription(
                Image,
                '/image_raw',
                self.image_callback,
                qos_profile_sensor_data)
        self.vehicle_cmd_subscription = self.create_subscription(TritonAIRacerControl,
                '/vehicle_cmd', self.vehicle_cmd_callback,1)
        self.saving_img = np.zeros((480, 640, 3))
        self.saving_throttle = 0
        self.saving_steering = 0.5
        self.filecounter = 0

    def image_callback(self, msg):
        # self.get_logger().info('I heard: "%s"' % msg.data)
        with lock:
            image = np.array(msg.data)
            image = image.reshape(480, 640, 3)
            self.saving_img = image
            saving_list = []
            saving_list.append(self.saving_img)
            saving_list.append(self.saving_throttle)
            saving_list.append(self.saving_steering)
            with open('recorded_data/saving_list'+ str(self.filecounter)  + '.obj', 'wb') as fp:
                pickle.dump(saving_list, fp)
            self.filecounter = self.filecounter + 1
            # plt.imshow(image)
            # plt.show()

    def vehicle_cmd_callback(self, msg):
        with lock:
            self.saving_throttle = msg.throttle.throttle
            self.saving_steering = msg.steering_openloop.steer

def main(args=None):
    rclpy.init(args=args)

    data_saver = Data_Saver()

    rclpy.spin(data_saver)

    data_saver.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
