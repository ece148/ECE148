# Final project for ECE 148 

### Team members 
* David He(ECE), 
* Nhat Tang(MAE), 
* Guro Drange Veglo(UPS)

## About
The car was programmed to autonomously run laps through lane detection while adding thottle when detecting red at a certain distance. The detection of red is done with machine learning. This was made to achieve alternating cars. 

## Nodes
* Cone / red detecting node
* Lane detection node
* Lane guidance node 

## Topics
* /centroid
* /camera/color/image_raw
* /pos_pixels_over_thresh
* /cmd_vel

# Dependencies
* Linux environment
* Jetson Nano 
* ROS2 environment or pull docker image: djnighti/ucsd_robocar
* Download repository: https://gitlab.com/ece148/ECE148.git
  > git clone https://gitlab.com/ece148/ECE148.git 

# Demo 
https://gitlab.com/ece148/ECE148/-/blob/main/Demo2.MP4





